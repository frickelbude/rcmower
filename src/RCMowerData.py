'''
Created on 26.05.2015

DataStream

ID   Count    Desc                Example - values are hex values without 0x
1    1        START_BYTE            0x7E
2    2        NUMBER_OF_VOLTS       0x0000-0xFF
3    1        NUMBER_OF_ACCEL       0x00-0xFF
4    1        NUMBER_OF_GPS         0x00-0xFF
5    1        NUMBER_OF_TEMP        0x00-0xFF
6    1        NUMBER_OF_DIGITAL     0x00-0xFF
7    1        NUMBER_OF_ANALOG      0x00-0xFF
---- per Volt entry two bytes
8    1        MSB Value volt1       0x00-0xFF    Vorkomma
9    1        LSB Value volt1       0x00-0xFF    Nachkomma
n+1
n+2
...
--- per Accelerator, two Bytes per Sensor
    1        MSB value AccelX        0x00-0xFF
    1        LSB value AccelX
    1        MSB value AccelY
    1        MSB value AccelY
    1        MSB value AccelZ
    1        MSB value AccelZ
--- Temp, two Bytes per Sensor
    1        MSB Temp1 (Vorkomma)
    1        LSB Temp1 (Nachkomma)




@author: bernhardklein
'''

class RCMowerData(object):
    '''
    classdocs
    '''

    MAX_STREAM_LENGTH = 50
    VERSION = "0.1g"
    
    VOLT={}             # 2 Bytes per Entry 
    ACCELERATE={}
    COMPASS={}
    GPS={}
    TEMPERATURE={}
    DIGITAL={}
    ANALOG={}
    
    START_BYTE = 0x7E
    SEP_BYTE = 0xFE
    END_BYTE = 0xFD
    
    def __init__(self):
        '''
        Constructor
        '''
     
    def version(self):
        return self.VERSION
       
    def addVolt(self, idx, value):
        if idx < 0:
            idx = 0
        if value > 65535:
            value = 65535
            
        word = [0,0]
        word[0] = ((value>>8) & 0xFF)
        word[1] = (value & 0xFF)        
        self.VOLT[idx] = word       
        pass
    
    def addButton(self, idx, value):
        if idx< 0:
            idx = 0
        if value > 0:
            value = 1
        if value < 0:
            value = 0
        self.BUTTON[idx] = (value & 0xFF)
        pass
    
    def addHat(self, idx, value):
        if idx< 0:
            idx = 0
        if value > 0:
            value = 1
        if value < 0:
            value = 0
        self.HAT[idx] = (value & 0xFF)
        pass
    
    def getJoysticks(self):
        return self.JOYSTICK
    
    def getJoystick(self, idx):
        return self.JOYSTICK[idx]
    
    def getButtons(self):
        return self.BUTTON
    
    def getButton(self, idx):
        return self.BUTTON[idx]
        
    def getHats(self):
        return self.HAT
    
    def getHat(self, idx):
        return self.HAT[idx]
    
    def setDataStream(self, stream):
        if (len(stream) != self.MAX_STREAM_LENGTH):
            return
        jl = stream[1]
        bl = stream[2]
        hl = stream[3]
        
        print "jl:%d, bl: %d, hl: %d" % (jl, bl, hl)
        
        # reininitialize internal dicts
        self.JOYSTICK = {}
        self.BUTTON = {}
        self.HAT = {}
        
        idx = 4
        for i in range (0,jl,1):
            print "JL (%d): %d" % (i, ((stream[idx+i] << 8) + stream[idx+i+1]))
            self.addJoystick(i, ((stream[idx+i] << 8) + stream[idx+i+1]))
            idx += 1
            
        idx = 4 + (2*jl) 
        for i in range (0,bl,1):
            print "BL (%d): %d" % (i, stream[idx+i])
            self.addButton(i, stream[idx+i])

        idx = 4 + (2*jl) + bl 
        for i in range (0,hl,1):
            print "HL (%d): %d" % (i, stream[idx+i])
            self.addHat(i, stream[idx+i])

        pass
    
    def getDataStream(self):
        STREAM=[0]*self.MAX_STREAM_LENGTH
        STREAM[0]= (self.START_BYTE & 0xFF)
        STREAM[1] = len(self.JOYSTICK)
        STREAM[2] = len(self.BUTTON)
        STREAM[3] = len(self.HAT)
        x=4
        for key in sorted(self.JOYSTICK):
            STREAM[x] = self.JOYSTICK[key][0]
            STREAM[x+1] = self.JOYSTICK[key][1]
            x += 2

        for key in sorted(self.BUTTON):
            STREAM[x] = self.BUTTON[key]
            x += 1

        for key in sorted(self.HAT):
            STREAM[x] = self.HAT[key]
            x += 1            
                  
        STREAM[len(STREAM)-1] = (self.END_BYTE & 0xFF)
        return STREAM
 