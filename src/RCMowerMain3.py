#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
Created on Thu Aug 13 22:24:17 2015

@author: bernhardklein
"""
import os
import sys
import time
import subprocess

from evdev import *
from time import sleep
from threading import Thread, Timer
import threading
from Queue import Queue
from Motors import Motors
#import logging
#import logging.config

from ShowInfoData import ShowInfoData, RepeatedTimer

debug_level = 4
VERSION='0.1.0'

PIN_A = 10
PIN_B = 9
PIN_BTN = 11

MOTOR_LEFT = 1
MOTOR_RIGHT = 2
BAUD = 57600

MOTOR_RESET_GPIO = 4
MOTOR_ERROR_GPIO = 17
SERIAL = "/dev/ttyAMA0"

ESTOP_LEFT = 'BTN_Y'
ESTOP_RIGHT = 'BTN_Z'

dev = InputDevice('/dev/input/event0')
events = Queue()

MAX_FWD_SPEED = 3200
MAX_BWD_SPEED = -3200

ADJUST_NULL_POINT = {"ABS_Z":-1,"ABS_Y":0,"ABS_X":0,"ABS_RZ":0}
# percentage of MAX 100% speed
speed_factor = [0.5,0.6,0.8,1.0]

# LEFT = Motor left, RIGHT = Motor right
# LIFT = mower up/down
# ENGINE = [True|FALSE] cumbustion engine on/off
# #
# in MIX-Mode: 
#   Left Motor (LeftMix)    = Y(CH1) + Z(CH2)
#   Right Motor (RightMix)  = CH1-CH2
#
#   Example:
#       Y = 255, Z=0
#       L+R = 255
#       Y=255, Z=100 (right)
#       L=255, R=155    => mower turn right
#
#       Y=255, Z=-255   => mower turn left
#       L=0, R = 255
motor_speed = {'REVERSE':[False, False],'FACTOR':1.0,'LEFT':0, 'RIGHT':0, 'LIFT':0, 'MIXER1':False, 'CH1':0,'CH2':0,'ENGINE':False,'CHANGED':False}

last_speed = 0


eStopButtons = {ESTOP_LEFT:True, ESTOP_RIGHT:True}
isEStop = False
globalStop = False

#logging.config.fileConfig('logging.properties', defaults={'logfilename': 'mower.log'}, disable_existing_loggers=False)        # create logger
#log = logging.getLogger(__name__)

info = ShowInfoData(PIN_A, PIN_B, PIN_BTN)

def doAction(action):
    global globalStop
    '''
        callback function for menu selection
    '''
    func = action[1]
    param = action[3]
    
    if globals().has_key(func):
        if (param != None):
            globals()[func](param)
        else:
            globals()[func]()
    else:
        info.lcd.message("unknown call",0)
        info.lcd.message(str(func),1,False)
        info.lcd.message(str(param),2,False)
        time.sleep(5)
        restartApp()
    pass

def startMenu():
    global info
    info.startupMenu(doAction)
    while True:
        time.sleep(0.1)
        pass
    pass

def run():
    global isEStop, motor_speed, globalStop
    motor_speed['FACTOR'] = speed_factor[2]
    motor_speed['MIXER1'] = True   
    info.show(0)
    print "RUN"
    lasttime = 0
    lasthb = 0
    while True:

#---------------------------------------------------
#
#   EMERGENCY = if no eStopButton pressed
#       stop mower, 
#       do not stop engine, 
#       do not lift up cutter bar
#---------------------------------------------------
                
        motor_speed['ENGINE'] = isEStop
        if (globalStop):
            print "global stop"
            restartApp()
            
        if isEStopActive():
            #print "EStopActive()"
            motors.Stop()
            motor_speed['LEFT'] = 0
            motor_speed['RIGHT'] = 0
            motor_speed['MIXER1'] = True
            motor_speed['CH1'] = 0
            motor_speed['CH2'] = 0           
            if isEStop == False:
                isEStop = True
                motor_speed['ENGINE'] = isEStop
                info.setSpeed(
                            [int(motor_speed['LEFT']), int(motor_speed['RIGHT'])], 
                            motor_speed['REVERSE'], 
                            [motor_speed['FACTOR'], motor_speed['MIXER1'], motor_speed['ENGINE']]
                            )
                info.show(0)
#---------------------------------------------------
#
#   do cutting and move mower
#
#---------------------------------------------------
        if not isEStopActive():
            if isEStop == True:
                isEStop = False
                motor_speed['ENGINE'] = isEStop
                info.setSpeed(
                            [int(motor_speed['LEFT']), int(motor_speed['RIGHT'])], 
                            motor_speed['REVERSE'], 
                            [motor_speed['FACTOR'], motor_speed['MIXER1'], motor_speed['ENGINE']]
                            )
                info.show(0)
                motors.Start()
                isEStop = False
                motor_speed['CHANGED'] = True
                
            if motor_speed['CHANGED']:
                info.setSpeed(
                            [int(motor_speed['LEFT']), int(motor_speed['RIGHT'])], 
                            motor_speed['REVERSE'], 
                            [motor_speed['FACTOR'], motor_speed['MIXER1'], motor_speed['ENGINE']]
                            )
                info.show(0)
                motors.Start()
                motor_speed['CHANGED'] = False
                motors.Go([int(motor_speed['LEFT']), int(motor_speed['RIGHT'])])
                #print "MAIN:", motor_speed
                #print "------------"

        if ((time.time() - lasttime) > 1):
            info.setSpeed(
                        [int(motor_speed['LEFT']), int(motor_speed['RIGHT'])], 
                        motor_speed['REVERSE'], 
                        [motor_speed['FACTOR'], motor_speed['MIXER1'], motor_speed['ENGINE']]
                        )
            info.show(0)
            lasttime = time.time()
            
        if ((time.time() - lasthb) > 0.25):
            info.setHeartBeat()
            lasthb = time.time()

    pass


#---------------------------------------------------
# adjust value to zero and move it to motor speed value
# joystick device deliver at center point a value 127
# 
# Step 1: this value is mapped to zero
# Step 2: map value to MAX_FWD_SPEED
# Step 3: check boundaries
# Step 4: adjust with speed factor
#---------------------------------------------------
def adjust(value, adjustvalue=127):
    value -= adjustvalue
    if (value > 0):
        value = map (value, 0,adjustvalue, 0, MAX_FWD_SPEED )
    if (value < 0):
        value = map (abs(value), 0,adjustvalue, 0, MAX_FWD_SPEED ) * -1
    # adjust with SPEED-Factor
    value = int(round((value * motor_speed['FACTOR']),0))
    return constrain(value,MAX_BWD_SPEED, MAX_FWD_SPEED)
    
#---------------------------------------------------
# map value in a min/max range into a new
# output ange out_min/out_max
#
# example :
# value = 127 (in_min=0, in_max=255)
# map to out_min=0, out_max=3200    => new value = 1600
#    
# value = 255 (in_min=0, in_max=255)
# map to out_min=0, out_max=3200    => new value = 3200
#   
#---------------------------------------------------
def map(value, in_min, in_max, out_min, out_max):
    return (value - in_min) * (out_max - out_min) / (in_max - in_min) + out_min


#---------------------------------------------------
# map value between a and b
#---------------------------------------------------
def constrain(value, a,b):
    if (value < a):
        return a
    elif (b < value):
        return b
    else:
        return value

#---------------------------------------------------
#
#   Analyse incomming event
#   only key or joystick events are used
#
#---------------------------------------------------
def analyseEvent(event):
    global motor_speed, globalStop
    if event.type in ecodes.bytype:
        codetype = ecodes.bytype[event.type][event.code]
        if event.code == 304:
            codetype = 'BTN_A'
    else:
        return
    
    value = event.value
    
    if codetype in [ESTOP_LEFT, ESTOP_RIGHT]:
        setEmergencyStop(codetype, event.value)
        return
    
    if codetype == "BTN_TL2":
        globalStop = False
        if isEStopActive():
            globalStop = True
            
    #-- check if Mixer ON/OFF
    if codetype == 'ABS_HAT0Y':
        if value == -1:
            motor_speed['MIXER1'] = True
            motor_speed['CHANGED'] = True
        if value == 1:
            motor_speed['MIXER1'] = False
            motor_speed['CHANGED'] = True

    #-- do we use a mixer ?
    if motor_speed['MIXER1'] == False:
        # -- user Y and RZ
        if codetype == 'ABS_Y':
            motor_speed['LEFT'] = adjust(value + ADJUST_NULL_POINT[codetype])
            motor_speed['CHANGED'] = True
        if codetype == 'ABS_RZ':
            motor_speed['RIGHT'] = adjust(value + ADJUST_NULL_POINT[codetype])
            motor_speed['CHANGED'] = True
    else:
        # -- MIX Y with Z
        if codetype == 'ABS_Y':
            motor_speed['CH1'] = adjust(value + ADJUST_NULL_POINT[codetype]) 
            motor_speed['LEFT']  = constrain((motor_speed['CH1'] + motor_speed['CH2']),
                                    MAX_BWD_SPEED,MAX_FWD_SPEED)
            motor_speed['RIGHT'] = constrain((motor_speed['CH1'] - motor_speed['CH2']),
                                    MAX_BWD_SPEED,MAX_FWD_SPEED)
            motor_speed['CHANGED'] = True
        if codetype == 'ABS_Z':
            motor_speed['CH2'] = adjust(value + ADJUST_NULL_POINT[codetype])            
            motor_speed['LEFT']  = constrain((motor_speed['CH1'] - motor_speed['CH2']),
                                    MAX_BWD_SPEED,MAX_FWD_SPEED)
            motor_speed['RIGHT'] = constrain((motor_speed['CH1'] + motor_speed['CH2']),
                                    MAX_BWD_SPEED,MAX_FWD_SPEED)
            motor_speed['CHANGED'] = True
            
        #print "BOTH MOTORS {} - {}".format(motor_speed['LEFT'], motor_speed['RIGHT'])        
    
    # -- adjust global speed with a speed factor (to reduce, increase maximum speed)
    if codetype in ['BTN_X', 'BTN_C', 'BTN_B', 'BTN_A']:        
        if (value == 1):
            new_value = 1.0
            if codetype == 'BTN_X':
               new_value = (speed_factor[0])
            if codetype == 'BTN_C':
               new_value = (speed_factor[1])       
            if codetype == 'BTN_B':
               new_value = (speed_factor[2])       
            if codetype == 'BTN_A':
               new_value = (speed_factor[3]) 
               
            if (new_value != motor_speed['FACTOR']) :               
                motor_speed['FACTOR'] = new_value
                #motor_speed['LEFT'] = int (round((motor_speed['LEFT'] * motor_speed['FACTOR']),0))
                #motor_speed['RIGHT'] = int (round((motor_speed['RIGHT'] * motor_speed['FACTOR']),0))
                motor_speed['CHANGED'] = True
        return
        
    #-- DEBUGGING ONLY - print out motor_speed
    if codetype == 'ABS_HAT0X':
        if value == 1:
            print 'DEBUG : >>> {}'.format( motor_speed)
        if value == -1:
            motor_speed = {'Reverse':[True, False], 'FACTOR':1.0,'LEFT':0, 'RIGHT':0, 'LIFT':0, 'MIXER1':False, 'CH1':0,'CH2':0,'ENGINE':False,'CHANGED':True}


    pass
      
 
def setCumbustionEngine(codetype,value):
    global motor_speed
    if codetype == 'BTN_TR2':
        if (value == 1 and not motor_speed['ENGINE'] ):
 #           log.debug("set engine on")
            motor_speed['ENGINE'] = True
            motor_speed['CHANGED'] = True
            
    if codetype == 'BTN_TL2':
        if (value == 1):
  #          log.debug("set engin off")
            motor_speed['ENGINE'] = False
            motor_speed['CHANGED'] = True

def getMessageRow(msg):
    return msg.keys()[0]
    
 
def setEmergencyStop(codetype, value):
    global eStopButtons
    eStopButtons[codetype] = True 
   # log.debug("{} - {}".format(eStopButtons[codetype],value))
    if value == 1:
        eStopButtons[codetype] = False
        #print eStopButtons[codetype]
         
def isEStopActive():
    global eStopButtons    
    return (eStopButtons[ESTOP_LEFT] and eStopButtons[ESTOP_RIGHT] )    
    
    

#---------------------------------------
# running as thread
# is current event a key or a joystick
#   put event into queue
#---------------------------------------      
def deviceListener():
        for event in dev.read_loop():
            if event.type == ecodes.EV_KEY or event.type == ecodes.EV_ABS:
                #events.put(event)
                analyseEvent(event)

        
        
#-----------------------------------------
# Callback function for MotorController
# is called, if Pololu driver detect an internal
# error
#
#-----------------------------------------
def MotorErrorCallback(errornr, message):
    print "ErrorCallback - Error code %d" % errornr
    print message
    pass

def shutdown():
    motors.Stop()
    motors.Shutdown()
    info.lcd.message("Shutdown...",0)
    sleep(2)
    info.lcd.clear()
    command = "/usr/bin/sudo /sbin/shutdown -r now"
    process = subprocess.Popen(command.split(), stdout=subprocess.PIPE)
    output = process.communicate()[0]
    print output
    info.lcd.message(output,0)

def rebootPI():
    motors.Stop()
    motors.Shutdown()
    info.lcd.message("reboot PI ...",0)
    sleep(2)
    info.lcd.clear()
    command = "/usr/bin/sudo reboot"
    process = subprocess.Popen(command.split(), stdout=subprocess.PIPE)
    output = process.communicate()[0]
    print output
    info.lcd.message(output,0)
    

def restartApp():
    """Restarts the current program.
    Note: this function does not return. Any cleanup action (like
    saving data) must be done before calling this function."""
    #python = sys.executable
    #os.execl(python, python, * sys.argv)
    info.lcd.message("restarting...",0)
    time.sleep(2)
    os.execv(__file__, sys.argv)

def heartbeat():
    info.setHeartBeat() 
    
    pass


def runMower():
    errcount = 0
    while errcount < 5:
        try:
            t = Thread(target=deviceListener)
            t.daemon = True
            t.start()
            run()
        except (KeyboardInterrupt, SystemExit):
            #sys.exit()
            print "die after CTRL-C interrupt"
            #hb.stop()
            shutdown()
            os._exit(0)
            
        except (AttributeError):
            print "restart"
            #restartApp()
            pass
    
        except IOError:
            errcount += 1
            info.lcd.message("{:1} retry read...".format(errcount),0)
            info.lcd.message("SonyF710 device",1)  
            time.sleep(3)
            t = None
        
    #
    #
    restartApp()
    
motors = Motors(MOTOR_LEFT, MOTOR_RIGHT, SERIAL, BAUD, MOTOR_RESET_GPIO, MOTOR_ERROR_GPIO, MotorErrorCallback)
motors.Stop()
motors.InverseMotors(True, False)
motor_speed["REVERSE"] = [True, False]
motor_speed["MIXER1"] = True

info.setInfoVersion(VERSION, "4.0.6-7")
info.show(2) 
time.sleep(3)
 

startMenu()
  

os._exit(0)  