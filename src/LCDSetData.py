# -*- coding: utf-8 -*-
"""
Created on Tue Sep 29 19:45:26 2015

@author: bernhardklein
"""

class LCDSetData(object):
    '''
        This class is used to set/get data dynamically during running
        your software.
        
        optimal usable wit LCDMenu to get/set data on LCD displays
    
    '''
    _dataStructure = None
    _call_draw = None

    _headerDisp = 0 
    _boolDisp=1
    _digitDisp=2
    _charDisp=3
    _stringDisp=4
    _displayString = [
        "*{:^14}*",
        #Example: <True >| False
        "{:^8}|{:^7}"
        
    ]  
    __DATA_BOOL = 0
    __DATA_DIGIT = 1
    __DATA_CHAR = 2
    __DATA_STR  =3
    
    _currentDataType = 0
    _numberOfRows = 0
    __dataDict = {0:False, 1:-999, 2:None, 3:None}
    
    __currentParameter = {
        0:[],
        1:[],
        2:[],
        3:[]
    }    
    
    def __init__(self, numberOfRows, draw_callback):
        self._dataStructure = {}
        self._call_draw = draw_callback
        self._numberOfRows = numberOfRows
        pass
    
    def _draw(self):
        pass
   
   
    def up(self, dataType):
        
        if dataType == self.__DATA_BOOL:
            print ">>>", self.__currentParameter
            b = self.__currentParameter[0][3][self.__currentParameter[0][4]]
            self.__currentParameter[0][3][self.__currentParameter[0][4]] = not b
            value = self.select_bool(
                self.__currentParameter[0][1],
                self.__currentParameter[0][2],
                self.__currentParameter[0][3],
                self.__currentParameter[0][4],           
            )       
        return value
    
    def down(self, dataType):
        if dataType == self.__DATA_BOOL:
            print ">>>", self.__currentParameter
            b = self.__currentParameter[0][3][self.__currentParameter[0][4]]
            self.__currentParameter[0][3][self.__currentParameter[0][4]] = not b
            value = self.select_bool(
                self.__currentParameter[0][1],
                self.__currentParameter[0][2],
                self.__currentParameter[0][3],
                self.__currentParameter[0][4],           
            )       
        return value
    
    def select_bool(self, label, param, data, key):
        '''
            display text and use can choose TRUE/FALSE

            Param:            
            label: text to display
            param: list with two choosable values
            
            
            return: True/False
            
        '''
        self.__currentParameter[self.__DATA_BOOL] = [self.__DATA_BOOL, label, param, data, key]
        MARKED1 = "<"
        MARKED2 = ">"
        
        disp = []
        self._currentDataType = self.__DATA_BOOL
        # header
        disp.append(
            self._displayString[self._headerDisp].format(label)
        )
        
        print "Param:", param
        print "Data: ", data
        print "Key:  ", key
        if self._numberOfRows <= 2:
            pass
        else:
            for i in range(len(param)):
                if param[i] == data[key]:
                    param[i] = MARKED1 + str(param[i]) + MARKED2
                    value = param
            disp.append(
                self._displayString[self._boolDisp].format(
                            str(param[0]),
                            str(param[1])
                )            
            )
            
            pass
        
        self._call_draw(disp)
        return [self.__DATA_BOOL, data]
        
        
    def select_digit(self, label, param):
        '''
            display label, user can choose digit(s)
            
            Param:
            label: text to display
            list with min/max value
        '''
        MARKED1 = "["
        MARKED2 = "]"
        
        disp = []
        self._currentDataType = self.__DATA_DIGIT
        # header
        disp.append(
            self._displayString[self._headerDisp].format(label)
        )
        
        print param
        print data
        print key
        if self._numberOfRows <= 2:
            pass
        else:
            for i in range(len(param)):
                if param[i] == data[key]:
                    param[i] = MARKED1 + str(param[i]) + MARKED2
            disp.append(
                self._displayString[self._boolDisp].format(
                            str(param[0]),
                            str(param[1])
                )            
            )
            
            pass
        
        value = False
        self._call_draw(disp)

        value = 0
        
        return [self._currentDataType, value]

    def select_char(self, label, param):
        '''
            display label, user can choose a char from param list
            
            Param:
            label: text to display
            list with possible characters to choose
        '''
        self._currentDataType = self.__DATA_CHAR
        value = ''
        
        return [self._currentDataType, value]
        
    def select_string(self, label, param):
        '''
            display label, one or more chars from param list
            all chars are concatinated into a string
            
            Param:
            label: text to display
            list with possible characters to choose
        '''
        self._currentDataType = self.__DATA_STR
        value = ""
        
        return [self._currentDataType, value]
        
    def select(self):
        return value