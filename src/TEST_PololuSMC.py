import sys

from Pololu_SMC import Pololu_SMC
from time import sleep

DRIVERID = 2

smc = None

def errorCallback(errornr, message):
    print "ErrorCallback - Error code %d" % errornr
    print message
    sys.exit(1)
    pass

def main(argv):
    smc = Pololu_SMC("/dev/ttyAMA0",57600, 4, 17, errorCallback)
    smc.ExitSafeStart(DRIVERID)
    sleep(1)
    print "---- TEST 1 - Forward "
    speed = 1000
    for x in range(1,20):
        smc.Forward(DRIVERID,speed)
        sleep(0.5)
        speed += 100
    
    print "slow down"
    for x in range(1,20):
        smc.Forward(DRIVERID, speed)
        sleep(0.2)
        speed -= 110
        
    print "BRAKE" 
    smc.Brake(DRIVERID, 10)  
    print "---- Test 2 - Backward"
    smc.Stop(DRIVERID)
    smc.ExitSafeStart(DRIVERID)
    sleep(0.5) 
    speed = 1000   
    for x in range(1,20):
        smc.Backward(DRIVERID, speed)
        sleep(0.5)
        speed += 100
    
    print "slow down"
    for x in range(1,20):
        smc.Backward(DRIVERID, speed)
        sleep(0.2)
        speed -= 110
        
    print "BRAKE" 
    smc.Brake(DRIVERID, 30)  
    sleep(0.5)    
    smc.Stop(DRIVERID)
    sleep(0.5)    
    smc.ExitSafeStart(DRIVERID)
    smc.Close()
    print "---------- finish -----------------"
    pass

#-------- MAIN METHODE -------------
if __name__ == '__main__':
    main(sys.argv[1:])
