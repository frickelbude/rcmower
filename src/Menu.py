# -*- coding: utf-8 -*-
"""
Created on Wed Aug 26 18:37:20 2015

@author: bernhardklein
"""
import copy

class Menu:  
    __menu_list = [
        # Status row <POWER> <ENGINE ON/OFF> <XBEE ON/OFF> <HEARTBEAT>
        #            0-100      E0/E1           X0/X1       —\|/
        "{:>3d}  E{:1d}  X{:1d}  {}",
        "L {:0>4d} R {:0>4d}",
        "HATX{:2d} HATY{:2d}",
        "BTNA {:1d} BTNB {:1d}",
        "BTNX {:1d} BTNY {:1d}",
        "TL {:1d} TR {:1d}",
        "LB {:1d} RB {:1d}",
        "X({:>4d}) Y({:>4d})",
        "Z({:>4d}) RZ({:>4d})",
        "Temp-ML {:5.1f}",
        "Temp-MR {:5.1f}",
        "TempCut {:5.1f}",
        "item 13",
        "item 14",
        "item 15",
        "item 16",
        "item 17",
        "item 18",
        "item 19",
        "item 20"
        
    ] 

#    __menu_list = [
#        # Status row <POWER> <ENGINE ON/OFF> <XBEE ON/OFF> <HEARTBEAT>
#        #            0-100      E0/E1           X0/X1       —\|/
#        "{:>3d}  E{:1d}  X{:1d}  {}",
#        "L {:0>4d} R {:0>4d}",
#        "HATX{:2d} HATY{:2d}",
#        "BTNA {:1d} BTNB {:1d}",
#        "BTNX {:1d} BTNY {:1d}",
#        "TL {:1d} TR {:1d}",
#        "LB {:1d} RB {:1d}",
#        "X({:>4d}) Y({:>4d})",
#        "Z({:>4d}) RZ({:>4d})",
#        "Temp-ML {:5.1f}",
#        "Temp-MR {:5.1f}",
#        "TempCut {:5.1f}",
#        "item 13",
#        "item 14",
#        "item 15",
#        "item 16",
#        "item 17",
#        "item 18",
#        "item 19",
#        "item 20"
#        
#    ]  
#    __menu_list = []
    data_list = []
    icon_up = ' ' #chr(94)
    icon_down = ' ' #'v'
    icon_sel =  '>'
    icon_end = ' '  #'-'
    icon_sep = ''
    
    click_callback = None
    draw_callback = None
    showNumberOfRows = 0
    currentPos = 0
    currentId = 0
    topRowWindow = 0
    isSelected = False
    
    def __init__(self, numberOfRows, click_call, draw_call):
        self.click_callback = click_call
        self.draw_callback = draw_call
        self.showNumberOfRows = numberOfRows
        self.data_list = list(self.__menu_list)
        pass
    
    def getMenuList(self):
        return copy.deepcopy(self.__menu_list)
    
    def setNumberOfRows(self, rows):
        self.showNumberOfRows = rows
        pass
    
    def setDataList(self,newData, doReset=True):
        self.data_list = copy.deepcopy(newData)
        #print self.data_list
        if (doReset):
            self.reset()
        else:
            self.draw()
        pass
    
    def up(self):
        self.isSelected = False
        if self.currentPos > 0:
            self.currentPos -= 1

        if self.currentPos == 0 and self.topRowWindow > 0:
            self.topRowWindow -= 1
            
        if self.currentId > 0:
            self.currentId -= 1
        self.draw()
        pass
    
    def down(self):
        self.isSelected = False
        if ((self.topRowWindow + self.showNumberOfRows) < len(self.data_list)):
            self.currentId += 1
            if (self.currentPos < self.showNumberOfRows):
                self.currentPos += 1
            if (self.currentId > (self.topRowWindow + self.showNumberOfRows)-1):
                self.topRowWindow += 1
        self.draw()
        pass
    
    def reset(self):
        self.isSelected = False
        self.topRowWindow = 0
        self.currentId = 0
        self.currentPos = 0
        self.draw()
        pass
    
    def draw(self):
        #print "TopRow ", self.topRowWindow," - ", (self.topRowWindow + self.showNumberOfRows)
        row = 0
        for x in range (self.topRowWindow, (self.topRowWindow + self.showNumberOfRows)):
            first = ' ' 
            # is this the first row of the display?
            if (x == self.topRowWindow):
                first = self.icon_up
            
            # is this the last row of the display?
            if (x == (self.topRowWindow + self.showNumberOfRows)-1):
                first = self.icon_down
                
            # is this first menu entry or we are at the end of the menu list
            if (x == 0 or x == len(self.data_list)):
                first = self.icon_end
                
            # get entry from menu list
            txt = self.data_list[x]
            
            
            if (x == self.currentId or (self.currentPos == 0 and x == 0)):
                first = self.icon_sel
                if (self.isSelected):
                    txt = '(' + self.data_list[x] + ')'
                    
            #
            text = first + self.icon_sep + txt
            msg = str(x) + ' ' + str(self.currentId) + ' ' + str(self.currentPos) + ' ' + str(self.topRowWindow) + ' ' + text
            #print msg 
            self.draw_callback(row, text)   
            row += 1            
            
    def click(self):
        self.isSelected = True
        self.click_callback(self.currentId)
        self.draw()
        pass
        
 



   