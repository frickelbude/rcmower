#!/usr/bin/python

import LCDDisplay

from subprocess import *
import sys
from time import sleep, strftime
from datetime import datetime
from rotaryencoder import RotaryEncoder
import RCMowerMain

PIN_A=10
PIN_B=9
PIN_KNOB=11


cmd = "ip addr show eth0 | grep inet | awk '{print $2}' | cut -d/ -f1"

MNU={0:' >>>> START <<<<',1:' **** STOP **** '}
ACT=0

def run_cmd(cmd):
    p = Popen(cmd, shell=True, stdout=PIPE)
    output = p.communicate()[0]
    return output

def startRCMower():
    cmd_rcmower = "sudo RCMowerMain.py"
    p = Popen(cmd_rcmower, shell=True, stdout=PIPE)
    p.communicate()[0]
    pass

def rotary_event(event):
    global ACT,MNU
    lcd.message(MNU[ACT],3,False)
    if event == RotaryEncoder.CLOCKWISE:
        ACT += 1
    elif event == RotaryEncoder.ANTICLOCKWISE:
        ACT -= 1
    elif event == RotaryEncoder.BUTTONDOWN:
        if ACT == 0:
            print "START mower"
            startRCMower()
    elif event == RotaryEncoder.BUTTONUP:
        if ACT == 1:
            print "STOP mower"
            lcd.clear()
            sys.exit(0)
              
    if ACT > len(MNU)-1:
        ACT=0
    if ACT < 0:
        ACT=len(MNU)-1
    pass

try:
    lcd = LCDDisplay.LCDDisplay()
    rotary = RotaryEncoder(PIN_A,PIN_B,PIN_KNOB,rotary_event)
    while 1:
        ipaddr = run_cmd(cmd)
        lcd.message("RC-Mower V 0.1",0)
        lcd.message(datetime.now().strftime('%b %d  %H:%M:%S'),1, False)
        lcd.message('IP {}'.format(ipaddr),2, False)
        lcd.message(MNU[ACT],3,False)
        sleep(2)
except KeyboardInterrupt:
    lcd.clear()
    sys.exit(0)
    