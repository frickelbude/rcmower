# -*- coding: utf-8 -*-
"""
ShowInfoData.py     => V1


@author: bernhardklein
"""

import copy
from LCDMenu import LCDMenu
import LCDDisplay
from rotaryencoder import RotaryEncoder
from lcdsites import LCDSites

from threading import Timer, Thread, Event

class RepeatedTimer(object):
    def __init__(self,t,hFunction, *args, **kwargs):
        self._timer       = None
        self.t            = t
        self.hFunction    = hFunction
        self.args         = args
        self.kwargs       = kwargs
        self.is_running   = False
        self.start()

    def _run(self):
        self.is_running = False
        self.start()
        self.hFunction(*self.args, **self.kwargs)

    def start(self):
        if not self.is_running:
            self._timer = Timer(self.t, self._run)
            self._timer.start()
            self.is_running = True
            
    def stop(self):
        self._timer.cancel()
        self.is_running = False
        

class ShowInfoData(object):
    count = 0
    menu = None
    lcd = None
    rotary = None
    _action = None
    _emergency_text = ""
    _akku = 0
    _xbee = 0
    _raw_data = [
    ]

    mower_sites = {
        "MOTOR":[
                "<MOTOR>  {d[0]}",
                "F:{d[0]}   Mix:{d[1]}",
                "LEFT ({d[1]}): {d[0]}",
                "RIGHT({d[1]}): {d[0]}"
                ],
                
        "XBEE": [
                "<< XBEE >>  {d[0]}",
                "RXD Bytes {d[0]}",
                "TXD Bytes {d[0]}",
                ""
                ],
        "INFO":[
                "RCMower {d[0]}",
                "(c) LunaX",
                "2014/2015",
                "Kernel {d[0]}"
        ]
    }

    mower_menu = [
        # [ parentLevelId, levelId, Title, [exec|sublevelId], param, data ]
        [-1,0,"MAIN",100, None],
    
        # ---- first menu level
        [0,100,"Start  mower","runMower",None],
        [0,100,"Config mower",200, None],
        [0,100,"Restart app","restartApp", None],
        [0,100,"Reboot  PI","rebootPI", None],
        [0,100,"Shutdown","shutdown", None],
    
        [100,200,"Motor",300, None],
        [100,200,"SonyF710",301, None],
        
        # ---- second menu levels
            
        [200,300,"Left motor",400, None],
        [200,300,"right motor",401, None],
        [200,300,"lifter motor",402, None],
    
        [200,301, "Set Factor",403, None] ,
        [200,301, "Set Joy",404, None] ,
    
        # ---- third menu levels
        # -------- Motors
        [300,400,"reverse","select_bool",["ON", "OFF"]],
        [300,400,"offset","select_digit",[-99,99]],
    
        [300,401,"reverse","select_bool",[True, False]],
        [300,401,"offset","select_digit",[-99,99]],
    
        [300,402,"reverse","select_bool",[True, False]],
        [300,402,"offset","select_digit",[-99,99]],
     
         # ------- SonyF710
        [301,403,"Factor 1","select_digit",[0,100]],
        [301,403,"Factor 2","select_digit",[0,100]],
        [301,403,"Factor 3","select_digit",[0,100]],
        [301,403,"Factor 4","select_digit",[0,100]],
    
        [301,404,"ABS X","select_digit",[-255,255]],
        [301,404,"ABS Y","select_digit",[-255,255]],
        [301,404,"ABS Z","select_digit",[-255,255]],
        [301,404,"ABS RZ","select_digit",[-255,255]]
     
    ]


    mower_data = {
        "MOTOR":{
                0:["{:>4}".format("RUN")],
                1:["{:>4.1f}".format(0.8),"{:>3}".format("OFF")],
                2:["{:>5}".format(-3200),"{:>1}".format("R")],
                3:["{:>5}".format(1920),"{:>1}".format("")]
                },
        "XBEE":{
                1:["{:>6}".format(2530)],
                2:["{:>6}".format(0)]
                },
                
        "INFO": {
                0:[],
                3:[],
                },


        # key = levelId + _ + index
        # value = real value for this data entry
        # "400_0":True  => left motor reverse=True
        # "400_1":0 => Left motor null adjust = 0
        "400_0":"OFF",
        "400_1":200,
        "401_0":True,
        "401_1":0,
        "402_0":False,
        "402_1":0,
        "403_0":50,
        "403_1":60,
        "403_2":80,
        "403_3":100,
        "404_0":10,
        "404_1":-5,
        "404_2":10,
        "404_3":0
    }

    menu_data = {
    }


    
    _menu_data = []
    _hb = ['\x03', '\x02']
    _hb_l = 0

    def __init__(self, pina,pinb,pinbtn):
        #self.menu = Menu(4, self._click_event, self._draw_event)
        self.lcd = LCDDisplay.LCDDisplay()
        self.rotary = RotaryEncoder(pina,pinb,pinbtn,self._rotary_event)
        #self._menu_data = copy.deepcopy(self._raw_data)

        self.sites = LCDSites(self._drawSite)
        self.sites.addNewSite(0,self.mower_sites['MOTOR'])
        self.sites.addNewSite(1,self.mower_sites['XBEE'])
        self.sites.addNewSite(2,self.mower_sites['INFO'])

        
        #self.rt = RepeatedTimer(1, self.setHeartBeat, None)
        #self.rt2 = RepeatedTimer(1,self.show, False)
        pass

    def setInfoFromEvent(self, event, codetype):
        pass

    def setHeartBeat(self):
        #print "."
        self.lcd.setCursor(15,0)
        self.lcd.writeChar(self._hb[self._hb_l])
        self._hb_l += 1
        if self._hb_l > len(self._hb)-1:
            self._hb_l = 0
        pass
    
    def _drawSite(self, lcddata):
        self.lcd.clear()
        row = 0
        for msg in lcddata:
            self.lcd.message(msg, row, False)
            row += 1
        pass
        
    def _drawmenu(self, disp):
        #print ">>>>>>>>>>", disp
        #print "--------------------"
        row=0
        self.lcd.clear()
        for msg in disp:
            #print "Row: {:<2} Text: {}".format(row, msg)
            self.lcd.message("{}".format(msg),row,False)
            row += 1
                
    def show(self, showSite=0):
        '''
            set a new entries into menu class
        '''
        self.sites.draw(showSite)
        pass
    
    def __setStatus(self, akku, emergency, xbee):        
        pass
        
    
    def setSpeed(self, speeds, dirs, status):
        '''
            speeds  => [left, right]
            dirs    => [""|"R", ""|"R"] => "R" = Reverse
            status  => [factor, mixed, run]
                        factor: 0.5 - 1.0
                        mixed : True/False
                        run   : True/False
        '''
        # -- set mixed True/False to ON/OFF        
        if status[1]:
            status[1]="ON"
        else:
            status[1]="OFF"
            
        # -- set run True/False to STOP/RUN        
        if status[2]:
            status[2]="STOP"
        else:
            status[2]="RUN"

            
        # -- set Reverse True/False to R/""
        for x in range(0,2):
            if (dirs[x]):
                dirs[x] = "R"
            else:
                dirs[x] = ""
                                
        self.mower_data["MOTOR"][0]=["{:>3}".format(status[2])]
        self.mower_data["MOTOR"][1]=["{:>4.1f}".format(status[0]),"{:>3}".format(status[1])]
        self.mower_data["MOTOR"][2]=["{:>6}".format(speeds[0]), "{:>1}".format(dirs[0])]
        self.mower_data["MOTOR"][3]=["{:>6}".format(speeds[1]), "{:>1}".format(dirs[1])]
        
        self.sites.updateSite(0, self.mower_data['MOTOR'], False)
        
        pass
       
    def setInfoVersion(self, version, kernel):
       self.mower_data["INFO"][0] = ["{:>8}".format(version)]
       self.mower_data["INFO"][3] = ["{:>9}".format(kernel)]
       
       self.sites.updateSite(2, self.mower_data["INFO"], True)
       pass
   
   
    def startupMenu(self, action_callback):
        self.menu = LCDMenu(4, self.mower_menu, self._drawmenu )
        self.menu.setCurrentLevelId(100)
        self.menu.draw(self.menu.getCurrentLevelId())
        self._actionCallback = action_callback
        pass
   
    #------------------------------------------------------
    # call back function from RotaryEncoder and Menu    
    #------------------------------------------------------
    def _rotary_event(self, event):
        '''
            callback function for rotary-encoer class
            depending on direction call menu up/down method
        '''
        self._action = None
        if event == RotaryEncoder.CLOCKWISE:
            print "Down"
            self.menu.down()
            pass
        elif event == RotaryEncoder.ANTICLOCKWISE:
            print "up"
            self.menu.up()
            pass
        elif event == RotaryEncoder.BUTTONDOWN:
            self._action = self.menu.select()
            print "Action:", self._action
            self._actionCallback(self._action)
            
        elif event == RotaryEncoder.BUTTONUP:
    		print "Button released"
        return   
     
    def getMenuAction(self):
        return self._action
       
#    def _draw_event(self, row, msg):
#        '''
#            called from menu class to display menu entries on output device
#            
#        '''
#        if row == 0:
#            self.lcd.message(msg, row)
#        else:
#            self.lcd.message(msg, row, False)
   