import sys

from Motors import Motors
from time import sleep

DLEFT = 0x1
DRIGHT = 0x2   #17dec

motors = None

def errorCallback(errornr, message):
    print "ErrorCallback - Error code %d" % errornr
    print message
    sys.exit(1)
    pass


def TEST1(motors):
    print "----- Speed up both motors -----"
    mlr= [0,0]
    speed = 0
    for x in range (1,15):
        mlr = [(1500+speed), (1500+speed)]
        motors.Go(mlr)
        speed += 100
        sleep(0.2)
        
    for x in range (1,15):
        mlr = [(1500+speed), (1500+speed)]
        motors.Go(mlr)
        speed -= 100
        sleep(0.2)
        
    sleep(1)
    motors.Stop()
    pass

def TEST2(motors):
    print "---------- GO LEFT ------------"
    motors.Go(1600)
    motors.Start()
    for a in range (9,0, -1):
        print " GoLeft (%d)" % a
        motors.GoLeft(a, 2500)
        sleep(1)
        
    pass

def TEST3(motors):
    print "---------- GO RIGHT ------------"
    motors.Go(1600)
    motors.Start()
    for a in range (9,0, -1):
        print " GoLeft (%d)" % a
        motors.GoRight(a, 2500)
        sleep(1)
        
        
    pass

def TEST4(motors):
    print "---------- TURN LEFT ------------"
    motors.Go(2500)
    motors.Start()
    for a in range (9,0, -1):
        print " GoLeft (%d)" % a
        motors.TurnLeft(a, 2500)
        sleep(1)       
    pass

def TEST5(motors):
    print "---------- TURN RIGHT ------------"
    motors.Go(2500)
    motors.Start()
    for a in range (9,0, -1):
        print " GoLeft (%d)" % a
        motors.TurnRight(a, 2500)
        sleep(1)       
    pass
     

def help():
    print "******************************************"
    print "TEST Motor.py module"
    print ""
    print "sudo python TEST_Motors.py [1|2|3|4|5]"
    print ""
    print "1 = Test 1 speed up both motors"
    print "2 = Test 2 go left"
    print "3 = Test 3 go right"
    print "4 = not implemented yet"
    print "5 = not implemented yet"
    print "******************************************"
    

def main(argv):
    motors = Motors(DLEFT,DRIGHT,"/dev/ttyAMA0",57600, 4, 17, errorCallback)
    motors.Stop()
    motors.InverseMotors(False, True)
    motors.Start()
    
    print "Sys:argv ", argv
    if (len (argv) == 0):
        help()
    for p in argv:
        if str(p) in ['1','2','3','4','5']:
            if p == '1':
                TEST1(motors)
            if p == '2':
                TEST2(motors)
            if p == '3':
                TEST3(motors)
            if p == '4':
                TEST4(motors)
            if p == '5':
                TEST5(motors)
     
    
    motors.Shutdown()
    print "---------- finish -----------------"
    pass

#-------- MAIN METHODE -------------
if __name__ == '__main__':
    main(sys.argv[1:])