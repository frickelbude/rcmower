# -*- coding: utf-8 -*-
"""

ShowInfoData.py => V0

@author: bernhardklein
"""

import copy
from Menu import Menu
import LCDDisplay
from rotaryencoder import RotaryEncoder

from threading import Timer, Thread, Event

class RepeatedTimer(object):
    def __init__(self,t,hFunction, *args, **kwargs):
        self._timer       = None
        self.t            = t
        self.hFunction    = hFunction
        self.args         = args
        self.kwargs       = kwargs
        self.is_running   = False
        self.start()

    def _run(self):
        self.is_running = False
        self.start()
        self.hFunction(*self.args, **self.kwargs)

    def start(self):
        if not self.is_running:
            self._timer = Timer(self.t, self._run)
            self._timer.start()
            self.is_running = True
            
    def stop(self):
        self._timer.cancel()
        self.is_running = False
        

class ShowInfoData(object):
    count = 0
    menu = None
    lcd = None
    rotary = None
    _emergency_text = ""
    _akku = 0
    _xbee = 0
    _raw_data = [
        "{} X{:<2d} P{:>4d}",           #0
        "L-Speed({}){:>5d}",
        "R-Speed({}){:>5d}",
        "Cutter:   {:>5d}",
        "MIXED:      {}",             
        "OFFSET    {:>3d}",             #5
        "Temp-ML:  {:5.1f}",
        "Temp-MR:  {:5.1f}",            
        "TempCut:  {:5.1f}"
            
#        "Joy-X:    {:>5d}",
#        "Joy-Y:    {:>5d}",             
#        "Joy-Z:    {:>5d}",
#        "Joy-RZ:   {:>5d}",
#        "HAT X:        {:2d}",
#        "HAT Y:        {:2d}",
#        "BTN-A:        {:1d}",          
#        "BNT_B:        {:1d}",
#        "BTN-C:        {:1d}",
#        "BTN-X:        {:1d}",
#        "BTN-Y:        {:1d}",
#        "BTN-TL:       {:1d}",          
#        "BTN-TR:       {:1d}",          
#        "BTN-LB:       {:1d}",
#        "BTN-RB:       {:1d}"
        ]
    
    _menu_data = []
    _hb = ['\x03', '\x02']
    _hb_l = 0

    def __init__(self, pina,pinb,pinbtn):
        self.menu = Menu(4, self._click_event, self._draw_event)
        self.lcd = LCDDisplay.LCDDisplay()
        self.rotary = RotaryEncoder(pina,pinb,pinbtn,self._rotary_event)
        self._menu_data = copy.deepcopy(self._raw_data)
        
        #self.rt = RepeatedTimer(1, self.setHeartBeat, None)
        #self.rt2 = RepeatedTimer(1,self.show, False)
        pass

    def stop(self):
        self.rt.stop()
        pass
    
    def setInfoFromEvent(self, event, codetype):
#        if codetype == 'BTN_A':
#            self._menu_data[10] = self._raw_data[10].format(event.value)
#        if codetype == 'BTN_B':
#            self._menu_data[11] = self._raw_data[11].format(event.value)
#        if codetype == 'BTN_C':
#            self._menu_data[12] = self._raw_data[12].format(event.value)
#        if codetype == 'BTN_X':
#            self._menu_data[13] = self._raw_data[13].format(event.value)
#        if codetype == 'BTN_Y':
#            self._menu_data[14] = self._raw_data[14].format(event.value)
#
#        if codetype == 'BTN_TL2':
#            self._menu_data[15] = self._raw_data[15].format(event.value)
#        if codetype == 'BTN_TR2':
#            self._menu_data[15] = self._raw_data[16].format(event.value)
#
#        if codetype == 'ABS_X':
#            self._menu_data[4] = self._raw_data[4].format(event.value)
#        if codetype == 'ABS_Y':
#            self._menu_data[5] = self._raw_data[5].format(event.value)
#
#        if codetype == 'ABS_Z':
#            self._menu_data[6] = self._raw_data[6].format(event.value)
#        if codetype == 'ABS_RZ':
#            self._menu_data[7] = self._raw_data[7].format(event.value)


        pass
    
    def _rotary_event(self, event):
        '''
            callback function for rotary-encoer class
            depending on direction call menu up/down method
        '''
        if event == RotaryEncoder.CLOCKWISE:
            self.menu.down()
        elif event == RotaryEncoder.ANTICLOCKWISE:
            self.menu.up()
        elif event == RotaryEncoder.BUTTONDOWN:
            print "set new data"
        elif event == RotaryEncoder.BUTTONUP:
    		print "Button released"
        return   
        
    def _click_event(self, menu_id):
        '''
            callback function for a clickevent. If switch pressed
            on rotary encoder this function is called
        '''
        pass
    
    def _draw_event(self, row, msg):
        '''
            called from menu class to display menu entries on output device
            
        '''
        if row == 0:
            self.lcd.message(msg, row)
        else:
            self.lcd.message(msg, row, False)
        pass

    def setHeartBeat(self):
        #print "."
        self.lcd.setCursor(15,0)
        self.lcd.writeChar(self._hb[self._hb_l])
        self._hb_l += 1
        if self._hb_l > len(self._hb)-1:
            self._hb_l = 0
        pass
    
    def show(self, doReset = True):
        '''
            set a new entries into menu class
        '''
        self.__setStatus(self._akku, self._emergency_text, self._xbee)
        self.menu.setDataList(self._menu_data, doReset)
        pass
    
    def __setStatus(self, akku, emergency, xbee):
        '''
            akku = 0-100
            emergency = string (STOP|RUN)
            xbee = 0|1
        '''
        self._emergency_text = emergency
        self._akku = akku
        self._xbee = xbee
        self._menu_data[0] = self._raw_data[0].format(self._emergency_text, xbee, akku)
        
        pass
    
    def setEmergencyInfo(self, emergency):
        if emergency == 1:
            self._emergency_text = "STOP"
        else:
            self._emergency_text = "RUN "
        pass
    
    def setPower(self, akku):
        self._akku = akku
        pass
    
    def setXbee(self, xbee):
        self._xbee = xbee
        pass
    
            
        
    
    def setSpeed(self, left, right, cutter, isMixed):
        '''
        '''
        m='-'
        self._menu_data[4] = self._raw_data[4].format('OFF')
        if isMixed == True:
            m='M'
            self._menu_data[4] = self._raw_data[4].format('ON')
            
        self._menu_data[1] = self._raw_data[1].format(m,left)
        self._menu_data[2] = self._raw_data[2].format(m,right)
        self._menu_data[3] = self._raw_data[3].format(cutter)


        pass
    
    
    def setHat(self, x,y):
        self._menu_data[4] = self._raw_data[4].format(x,y)
        pass
    
    def setJoyXY(self, x,y):
        self._menu_data[11] = self._raw_data[11].format(x)
        self._menu_data[12] = self._raw_data[12].format(y)
        pass
    
    def setJoyZRZ(self, z,rz):
        self._menu_data[13] = self._raw_data[13].format(z)
        self._menu_data[14] = self._raw_data[14].format(rz)
        pass
    
    def setBtnAB(self, a,b):
        self._menu_data[5] = self._raw_data[5].format(a,b)
        pass
    
    def setBtnLBRB (self, lb,rb):
        self._menu_data[9] = self._raw_data[9].format(lb)
        self._menu_data[10] = self._raw_data[10].format(rb)
        pass
    
    def setBtnTLTR(self, tl,tr):
        self._menu_data[7] = self._raw_data[7].format(tl)
        self._menu_data[8] = self._raw_data[8].format(tr)
        pass
    
    def setBtnXY (self, x,y):
        self._menu_data[6] = self._raw_data[6].format(x,y)
        pass
 
    def setTempMotorLeft (self, t):
        self._menu_data[15] = self._raw_data[15].format(t)
        pass
   
    def setTempMotorRight (self, t):
        self._menu_data[16] = self._raw_data[16].format(t)
        pass
   
    def setTempMotorCutterBar (self, t):
        self._menu_data[17] = self._raw_data[17].format(t)
        pass
   

   