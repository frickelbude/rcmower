'''
Created on 30.05.2015

@author: bernhardklein
'''

import pygame
import sys
from time import sleep
import RPi.GPIO as GPIO
import wiringpi2
import logging
import logging.config
from printf import printf
from TranceiverData import TranceiverData
from threading import Timer, Thread, Event

class TimerSequence():
    '''
       Timer-Thread, can be used for cyclic callbacks
       
    '''
    def __init__(self,t,hFunction):
        '''
            Constructor 
            
            t = scheduling in seconds
            hFunction = callback function
        '''
        self.t=t
        self.hFunction = hFunction
        self.thread = Timer(self.t,self.handle_function)

    def handle_function(self):
        self.hFunction()
        self.thread = Timer(self.t,self.handle_function)
        self.thread.start()

    def start(self):
        self.thread.start()

    def cancel(self):
        self.thread.cancel()
      

def callback_send():
    pass

        
#---------------------------------------------------------------------
# start system        
#---------------------------------------------------------------------
def main(argv):
    logging.config.fileConfig('logging.properties', defaults={'logfilename': "ps3controller.log"}, disable_existing_loggers=False)        # create logger
    logger = logging.getLogger('__name__')
    
    logger.info('**************************************************')
    logger.info('Start PiPS3Controller DEMO ')
    logger.info("(c) LunaX 2015")
    logger.info('**************************************************')
    
    
    pygame.init()
    
    ps3 = pygame.joystick.Joystick(0)
    ps3.init()

    data = TranceiverData()
    
    t_send = TimerSequence(callback_send)
    while(True):
        pass
        

#-------- MAIN METHODE -------------
if __name__ == '__main__':
    main(sys.argv[1:])