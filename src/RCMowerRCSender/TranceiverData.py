'''
Created on 22.05.2015


Datenstream:

ID   Count    Desc                Example - values are hex values without 0x
1    1        START_BYTE          0x7E
2    1        Number of axis      4
3    1        Number of buttons   8
4    1        Number of hats      0  
5    2        Value AXIS 0        0x0000 ... 0xFFFF
6    2        Value AXIS 1        0x0000 ... 0xFFFF
7    2        Value AXIS 2        0x0000 ... 0xFFFF
8    2        Value AXIS 3        0x0000 ... 0xFFFF
9    1        Button1             0 ... 1
10   1        Button2             0 ... 1
11   1        Button3             0 ... 1
12   1        Button4             0 ... 1
13   1        Button5             0 ... 1
14   1        Button6             0 ... 1
15   1        Button7             0 ... 1
16   1        Button8             0 ... 1
.             FILL-BYTE           0
.
.
x   1        END_BYTE            FD
---------------------------------------------------
    30        Zeichen

Byte1:    StartByte    => FF
Byte2-5:    Axis1        => 0000 .... FFFF


@author: bernhardklein
'''
from _curses import version

class TranceiverData(object):
    '''
    classdocs
    '''

    MAX_STREAM_LENGTH = 30
    VERSION = "0.1g"
    
    JOYSTICK = {}
    BUTTON={}
    HAT={}
    
    START_BYTE = 0x7E
    SEP_BYTE = 0xFE
    END_BYTE = 0xFD
    
    def __init__(self):
        '''
        Constructor
        '''
     
    def version(self):
        return self.VERSION
       
    def addJoystick(self, idx, value):
        '''
        insert/update for Joystick idx a value.
        All values are splitted into MSB & LSB value. 
        Max value is 32767
        
        '''
        if idx < 0:
            idx = 0
        if value > 32767:
            value = 32767
            
        word = [0,0]
        word[0] = ((value>>8) & 0xFF)
        word[1] = (value & 0xFF)        
        self.JOYSTICK[idx] = word       
        pass
    
    def addButton(self, idx, value):
        '''
        Insert/update for button idx the value
        '''
        if idx< 0:
            idx = 0
        if value > 0:
            value = 1
        if value < 0:
            value = 0
        self.BUTTON[idx] = (value & 0xFF)
        pass
    
    def addHat(self, idx, value):
        '''
        Insert/update for hat idx the value
        '''
        if idx< 0:
            idx = 0
        if value > 0:
            value = 1
        if value < 0:
            value = 0
        self.HAT[idx] = (value & 0xFF)
        pass
    
    def getJoysticks(self):
        '''
        return joxstick array
        '''
        return self.JOYSTICK
    
    def getJoystick(self, idx):
        '''
        return value for joystick idx
        '''
        return self.JOYSTICK[idx]
    
    def getButtons(self):
        '''
        return button array
        '''
        return self.BUTTON
    
    def getButton(self, idx):
        '''
        return value for button idx
        '''
        return self.BUTTON[idx]
        
    def getHats(self):
        '''
        return hat array
        '''
        return self.HAT
    
    def getHat(self, idx):
        '''
        return value for hat idx
        '''
        return self.HAT[idx]
    
    def setDataStream(self, stream):
        '''
        convert stream into joystick, button, hat values.
        Initialize all internal arrays.
        '''
        if (len(stream) != self.MAX_STREAM_LENGTH):
            return
        jl = stream[1]
        bl = stream[2]
        hl = stream[3]
        
        print "jl:%d, bl: %d, hl: %d" % (jl, bl, hl)
        
        # reininitialize internal dicts
        self.JOYSTICK = {}
        self.BUTTON = {}
        self.HAT = {}
        
        idx = 4
        for i in range (0,jl,1):
            print "JL (%d): %d" % (i, ((stream[idx+i] << 8) + stream[idx+i+1]))
            self.addJoystick(i, ((stream[idx+i] << 8) + stream[idx+i+1]))
            idx += 1
            
        idx = 4 + (2*jl) 
        for i in range (0,bl,1):
            print "BL (%d): %d" % (i, stream[idx+i])
            self.addButton(i, stream[idx+i])

        idx = 4 + (2*jl) + bl 
        for i in range (0,hl,1):
            print "HL (%d): %d" % (i, stream[idx+i])
            self.addHat(i, stream[idx+i])

        pass
    
    def getDataStream(self):
        '''
        return a serializable stream of all joysticks, buttons, hat values
        Format see class doc
        '''
        STREAM=[0]*self.MAX_STREAM_LENGTH
        STREAM[0]= (self.START_BYTE & 0xFF)
        STREAM[1] = len(self.JOYSTICK)
        STREAM[2] = len(self.BUTTON)
        STREAM[3] = len(self.HAT)
        x=4
        for key in sorted(self.JOYSTICK):
            STREAM[x] = self.JOYSTICK[key][0]
            STREAM[x+1] = self.JOYSTICK[key][1]
            x += 2

        for key in sorted(self.BUTTON):
            STREAM[x] = self.BUTTON[key]
            x += 1

        for key in sorted(self.HAT):
            STREAM[x] = self.HAT[key]
            x += 1            
                  
        STREAM[len(STREAM)-1] = (self.END_BYTE & 0xFF)
        return STREAM
        