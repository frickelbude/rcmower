#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
Created on Thu Aug 13 22:24:17 2015

@author: bernhardklein
"""
import os
from evdev import *
from time import sleep
from threading import Thread, Timer
import threading
from Queue import Queue
from Motors import Motors
import logging
import logging.config

from ShowInfoData import ShowInfoData, RepeatedTimer

debug_level = 4
VERSION='0.1'

PIN_A = 10
PIN_B = 9
PIN_BTN = 11

MAX_FWD_SPEED = 3200
MAX_BWD_SPEED = -3200

MOTOR_LEFT = 1
MOTOR_RIGHT = 2
BAUD = 57600

MOTOR_RESET_GPIO = 4
MOTOR_ERROR_GPIO = 17
SERIAL = "/dev/ttyAMA0"

ESTOP_LEFT = 'BTN_Y'
ESTOP_RIGHT = 'BTN_Z'

dev = InputDevice('/dev/input/event0')
events = Queue()

# percentage of MAX 100% speed
speed_factor = [50,60,80,100]

# LEFT = Motor left, RIGHT = Motor right
# LIFT = mower up/down
# ENGINE = [True|FALSE] cumbustion engine on/off
# #
# in MIX-Mode: 
#   Left Motor (LeftMix)    = Y(CH1) + Z(CH2)
#   Right Motor (RightMix)  = CH1-CH2
#
#   Example:
#       Y = 255, Z=0
#       L+R = 255
#       Y=255, Z=100 (right)
#       L=255, R=155    => mower turn right
#
#       Y=255, Z=-255   => mower turn left
#       L=0, R = 255
motor_speed = {'FACTOR':0,'LEFT':0, 'RIGHT':0, 'LIFT':0, 'MIXER1':False, 'CH1':0,'CH2':0,'ENGINE':False,'CHANGED':False}

last_speed = 0

motor_speed['FACTOR'] = (speed_factor[1] / 100.0)

eStopButtons = {ESTOP_LEFT:True, ESTOP_RIGHT:True}
isEStop = False

logging.config.fileConfig('logging.properties', defaults={'logfilename': 'mower.log'}, disable_existing_loggers=False)        # create logger
log = logging.getLogger(__name__)

info = ShowInfoData(PIN_A, PIN_B, PIN_BTN)
def run():
    global isEStop, motor_speed
    info.show(True)
    print "RUN"
    while 1:
        
        #if not events.empty():
       #     event = events.get_nowait()
            #analyseEvent(event)

#---------------------------------------------------
#
#   EMERGENCY = if no eStopButton presse
#       stop mower, 
#       do not stop engine, 
#       do not lift up cutter bar
#---------------------------------------------------

        if isEStopActive():
            #print "EStopActive()"
            motors.Stop()
            motor_speed['LEFT'] = 0
            motor_speed['RIGHT'] = 0
            motor_speed['MIXER1'] = False
            motor_speed['CH1'] = 0
            motor_speed['CH2'] = 0           
            if isEStop == False:
                info.setEmergencyInfo(1)
                info.setSpeed(
                            int(motor_speed['LEFT']), 
                            int(motor_speed['RIGHT']), 
                            int(motor_speed['LIFT']), 
                            bool(motor_speed['MIXER1'])  )
                isEStop = True
                info.show()
#---------------------------------------------------
#
#   do cutting and move mower
#
#---------------------------------------------------
        if not isEStopActive():
            if isEStop == True:
                motors.Start()
                #log.warn("ESTOP-Buttons activated")
                isEStop = False
                motor_speed['CHANGED'] = True
                info.setEmergencyInfo(0)
                info.setSpeed(
                            int(motor_speed['LEFT']), 
                            int(motor_speed['RIGHT']), 
                            int(motor_speed['LIFT']), 
                            bool(motor_speed['MIXER1'])  )
                info.show()
                
            if motor_speed['CHANGED']:
                
                motors.Start()
                #log.debug("SPEED {}".format(motor_speed))
                motor_speed['CHANGED'] = False
                #print motor_speed
                info.setSpeed(
                            int(motor_speed['LEFT']), 
                            int(motor_speed['RIGHT']), 
                            int(motor_speed['LIFT']), 
                            bool(motor_speed['MIXER1'])  )
                info.show()
                motors.Go([int(motor_speed['LEFT']), int(motor_speed['RIGHT'])])
                #print motor_speed['LEFT'],motor_speed['RIGHT']


#---------------------------------------------------
#
#   Analyse incomming event
#   only key or joystick events are used
#
#---------------------------------------------------
       
def analyseEvent(event):
    global motor_speed
    if event.type in ecodes.bytype:
        codetype = ecodes.bytype[event.type][event.code]
        if event.code == 304:
            codetype = 'BTN_A'
    else:
        codetype = "?"
    
    #print event.code
#    log.debug("EventCode(" + str(event.code) + ") codetype (" + codetype + ") value(" + str(event.value) + ")")

    if codetype in [ESTOP_LEFT,ESTOP_RIGHT]:
        setEmergencyStop(codetype, event.value)
        return
            
    if codetype in ['ABS_HAT0Y'] or event.code == 1:
        if (event.value == -1):
            motor_speed['MIXER1'] = True        
        if (event.value == 1):
            motor_speed['MIXER1'] = False        
        motor_speed['CHANGED'] = True    
        #print "MIXED {}".format(motor_speed['MIXER1'])
        
            
    if codetype in ['ABS_Y', 'ABS_RZ','ABS_Z']:
        if  motor_speed['MIXER1'] == False and codetype in ['ABS_Y', 'ABS_RZ']:
            setSpeed(codetype, event.value)
        if  motor_speed['MIXER1'] == True and codetype in ['ABS_Y', 'ABS_Z']:
            setMixedSpeed(codetype, event.value)
            
    if codetype in ['BTN_X', 'BTN_C', 'BTN_B', 'BTN_A']:
        if (event.value == 1):
            setOffsetSpeed(codetype)
                
    if codetype in ['BTN_TL2','BTN_TR2']:
        setCumbustionEngine(codetype, event.value)
        
    #-------------------------------------------------------------------------    
    # ONLY FOR DEBUGGING
    #-------------------------------------------------------------------------    
    if codetype == 'ABS_HAT0X':
        if (event.value == -1):
            print ">> ", motor_speed
            return
        if (event.value == 1):
            motor_speed['LEFT'] = 0
            motor_speed['RIGHT'] = 0
            motor_speed['CH1'] = 0
            motor_speed['CH2'] = 0
            motor_speed['MIXER1'] = 0
            motor_speed['CHANGED'] = True


    #info.setInfoFromEvent(event, codetype)
    pass
 
def setCumbustionEngine(codetype,value):
    global motor_speed
    if codetype == 'BTN_TR2':
        if (value == 1 and not motor_speed['ENGINE'] ):
 #           log.debug("set engine on")
            motor_speed['ENGINE'] = True
            motor_speed['CHANGED'] = True
            
    if codetype == 'BTN_TL2':
        if (value == 1):
  #          log.debug("set engin off")
            motor_speed['ENGINE'] = False
            motor_speed['CHANGED'] = True

def getMessageRow(msg):
    return msg.keys()[0]
    
def setMixedSpeed(codetype, value):
    global motor_speed 
    # joy stick to drive left/right
    
    # ABS_Z = CH2
    if codetype == 'ABS_Z':
        if (value == 127):
            motor_speed['CH2'] = 0
            motor_speed['LEFT'] =  motor_speed['CH1'] - motor_speed['CH2']
            motor_speed['RIGHT'] = motor_speed['CH1'] + motor_speed['CH2']
            #print "Z-127"
            
        if (value < 127):
            motor_speed['CH2']  = map(value,127,0,0,MAX_FWD_SPEED)
            motor_speed['LEFT'] =  motor_speed['CH1'] - motor_speed['CH2']
            motor_speed['RIGHT'] = motor_speed['CH1'] + motor_speed['CH2']
                                    
            # -- check range for forward                     
            motor_speed['LEFT'] = constrain(motor_speed['LEFT'], 0, MAX_FWD_SPEED)
            motor_speed['RIGHT'] = constrain(motor_speed['RIGHT'], 0, MAX_FWD_SPEED)
        elif (value > 127):
            motor_speed['CH2']  = map(value,127,255,0,MAX_FWD_SPEED)
            motor_speed['LEFT'] = motor_speed['CH1'] + motor_speed['CH2']
            motor_speed['RIGHT'] = motor_speed['CH1'] - motor_speed['CH2']
                                    
            # -- check range for backward
            motor_speed['LEFT'] = constrain(motor_speed['LEFT'], 0, MAX_FWD_SPEED)
            motor_speed['RIGHT'] = constrain(motor_speed['RIGHT'], 0, MAX_FWD_SPEED)


    # ABS_Y = CH1
    if codetype == 'ABS_Y':
        if (value == 127):
            motor_speed['CH1'] = 0
            motor_speed['LEFT'] =  motor_speed['CH1'] - motor_speed['CH2']
            motor_speed['RIGHT'] = motor_speed['CH1'] + motor_speed['CH2']
            
        if (value < 127):
            motor_speed['CH1']  = map(value,127,0,0,MAX_FWD_SPEED)
            motor_speed['LEFT'] =  motor_speed['CH1'] - motor_speed['CH2']
            motor_speed['RIGHT'] = motor_speed['CH1'] + motor_speed['CH2']
               
            # -- check range for forward                     
            motor_speed['LEFT'] = constrain(motor_speed['LEFT'], 0, MAX_FWD_SPEED)
            motor_speed['RIGHT'] = constrain(motor_speed['RIGHT'], 0, MAX_FWD_SPEED)
        elif (value > 127):
            motor_speed['CH1']  = map(value,127,255,0,MAX_BWD_SPEED)
            motor_speed['LEFT'] = motor_speed['CH1'] + motor_speed['CH2']
            motor_speed['RIGHT'] = motor_speed['CH1'] - motor_speed['CH2']
            
            # -- check range for backward
            motor_speed['LEFT'] = constrain(motor_speed['LEFT'], 0, MAX_BWD_SPEED)
            motor_speed['RIGHT'] = constrain(motor_speed['RIGHT'], 0, MAX_BWD_SPEED)
            
    
    # -- adjust speed with current speed factor    
    motor_speed['LEFT'] = int(round((motor_speed['LEFT'] * motor_speed['FACTOR']),0))  
    motor_speed['RIGHT'] = int(round((motor_speed['RIGHT'] * motor_speed['FACTOR']),0))  
    motor_speed['CHANGED'] = True
    pass     
 
def setSpeed(codetype, value):
    global motor_speed 
    key = 'LEFT' if codetype == 'ABS_Y' else 'RIGHT'
    if (value <= 127):
        motor_speed[key] = map(value,127,0,0,MAX_FWD_SPEED)
    else:
        motor_speed[key] = map(value,127,255,0,MAX_BWD_SPEED)
            
    #
    # change speed depending on speed_factor
    # MAX-speed is only available if speed_factor = 1.0
    #
    motor_speed[key] = int(round((motor_speed[key] * motor_speed['FACTOR']),0))  
    motor_speed['CHANGED'] = True
    motor_speed['LIFT'] = 0
    pass

def setEmergencyStop(codetype, value):
    global eStopButtons
    eStopButtons[codetype] = True 
   # log.debug("{} - {}".format(eStopButtons[codetype],value))
    if value == 1:
        eStopButtons[codetype] = False
        #print eStopButtons[codetype]
         
def isEStopActive():
    global eStopButtons    
    return (eStopButtons[ESTOP_LEFT] and eStopButtons[ESTOP_RIGHT] )    
    
    
def setOffsetSpeed(codetype):
    global motor_speed
    if codetype == 'BTN_X':
       motor_speed['FACTOR'] = (speed_factor[0] / 100.0)
    if codetype == 'BTN_C':
       motor_speed['FACTOR'] = (speed_factor[1] / 100.0)       
    if codetype == 'BTN_B':
       motor_speed['FACTOR'] = (speed_factor[2] / 100.0)       
    if codetype == 'BTN_A':
       motor_speed['FACTOR'] = (speed_factor[3] / 100.0)    
       
    motor_speed['LEFT'] = round((motor_speed['LEFT']  * motor_speed['FACTOR']),0)
    motor_speed['RIGHT'] = round((motor_speed['RIGHT']  * motor_speed['FACTOR']),0)
    motor_speed['CHANGED'] = True
    #print motor_speed
    #log.debug("new speed factor (" + str(motor_speed['FACTOR']) + ")")
       
 

#
# map input value to output value between min and max
#
# Example:
#   in = 128 (min=0, max=255)
#   out = 1500 (min=0, max=3000)
#
def map(value, in_min, in_max, out_min, out_max):
    return (value - in_min) * (out_max - out_min) / (in_max - in_min) + out_min

def constrain(value, a,b):
    if (value < a):
        return a
    elif (b < value):
        return b
    else:
        return value


#---------------------------------------
# running as thread
# is current event a key or a joystick
#   put event into queue
#---------------------------------------      
def deviceListener():
    for event in dev.read_loop():
        if event.type == ecodes.EV_KEY or event.type == ecodes.EV_ABS:
            #events.put(event)
            analyseEvent(event)


#-----------------------------------------
# Callback function for MotorController
# is called, if Pololu driver detect an internal
# error
#
#-----------------------------------------
def MotorErrorCallback(errornr, message):
    print "ErrorCallback - Error code %d" % errornr
    print message
    pass

def shutdown():
    motors.Stop()
    motors.Shutdown()
    info.lcd.message("Shutdown...",0)
    sleep(2)
    info.lcd.clear()

def heartbeat():
    info.setHeartBeat() 
    
    pass

motors = Motors(MOTOR_LEFT, MOTOR_RIGHT, SERIAL, BAUD, MOTOR_RESET_GPIO, MOTOR_ERROR_GPIO, MotorErrorCallback)
motors.Stop()
motors.InverseMotors(True, False)

try:
    t = Thread(target=deviceListener)
    t.daemon = True
    t.start()
    run()
except (KeyboardInterrupt, SystemExit):
    #sys.exit()
    print "die after CTRL-C interrupt"
    #hb.stop()
    shutdown()
    info.stop()
    os._exit(0)