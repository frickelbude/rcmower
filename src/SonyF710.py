
"""
SonyF710 - specialized input device

This is a python interface to the Linux input system's event device and
specilized for SonyF710 gamepad devices.

General described unix event names would be wrapped into SonyF710 device names

This class provide button and joystick methods

Copyright (C) 2015 LunaX (berndklein42@gmail.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

"""

import Device
import Event
import os

class SonyF710(Device.Device):
    '''
        specialized class to handle a Sony Gamepad F710
        
    '''
    JOY_X = "ABS_X"
    JOY_Y = "ABS_Y"
    JOY_Z = "ABS_Z"
    JOY_RZ = "ABS_RZ"
    
    BTN_X="BTN_A"
    BTN_Y="BTN_X"
    BNT_A="BTN_B"
    BTN_B="BTN_C"
    
    BTN_LT="BTN_TL"
    BTN_RT="BTN_TR"
    BTN_LB="BTN_Y"
    BTN_RB="BTN_Z"
    
    def __init__(self, event):
        super(self.__class__,self).__init__(event)
     
    def poll(self):
        while 1:
            try:
                buffer = os.read(self.fd, self.packetSize)
            except OSError:
                return
            self.update(Event(unpack=buffer))
            
                     
    def __repr__(self):
        return "<Sony F710 axes=%r buttons=%r>" % (
            self.axes, self.buttons)


        
    def isButton(self, button):
        if self.buttons[button] == 1:
            return True
        
        return False
    
    def getJoystickValue(self, joystick, map):
        value = self.axes[joystick]
        if map.count() == 4:
            value = self.map(value, map[0], map[1], map[2], map[3])
        return value
    
    
    def map(self, value, in_min, in_max, out_min, out_max):
        return (value - in_min) * (out_max - out_min) / (in_max - in_min) + out_min
    
    
### The End ###