# -*- coding: utf-8 -*-
"""
Created on Wed Sep 16 12:22:54 2015

@author: bernhardklein
"""

class MenuNode(dict):
    '''
        modified dictionary with double keys.
        
        if a key is found in current dictionary, the value is stored inside
        a list
    '''
    def __setitem__(self, key, value):
        try:
            #
            # try to insert this key
            self[key]
        except KeyError:
            #
            # key available, create an item with a list value
            super(MenuNode, self).__setitem__(key, [])
        #
        # append this value into list
        self[key].append(value)

class LCDMenu(object):

    """
    Menu System typically for 2 or more lines. The real drawing on LCD
    is done via the draw_callback function. This class contain only menu
    handling. (MVC Pattern)    
    
    Structure is list of menu items with following format :
        item0       item1        item2  item3       item4
    [parentLevelId, levelId, Title, Identifier, values]
    
    parentLevelId   id of the parent level
    levelId:        level id. All nodes in this level have the same levelId 
    Title:          Text to display
    Identifier:     Integer or String
                    If positive integer:
                        => ReferenceID for SubMenu
                    If string:
                        => calling function to be returned if selected
    values:         if Identifier = string  and value != None
                        than values contain a selectable choosing list of
                            possible values for this entry
                            
                    string: must be:
                        select_bool => for choosing one of two (ON/OFF, YES/NO, ...)

                        select_digit => choosing a digit value between range
                                        of [min,max]
                                        
                        select_string => displaying a submenu 
                                        1. row = possible chars - selectable
                                                    left/right click (up/down)
                                        2. row = displaying selected values
                                        
                        select_ip   => 
    
    
    
    
    
    """
    _currentLevelId = 0
    _currentNodeIndx = 0
    _currentSelectedId = 0
    _selectChar=">"
    _parentIDs = {}
    _headerDisp = 0
    _firstDispEntry = _headerDisp + 1
    _errorDisp1 = 2
    _errorDisp2 = _errorDisp1 + 1
    _errorDisp3 = _errorDisp2 + 1
    _debugDisp1 = _errorDisp3 + 1
    _debugDisp2 = _debugDisp1 + 1
    _debugDisp3 = _debugDisp2 + 1
    _debugDisp4 = _debugDisp3 + 1
    
    _displayString = [
        "{:^3} {:<8}  {:>2}",
        "{:<1}{:<2} {:<10}{:>2}",
        "ERROR     ID:{:0>3}",
        ">>{:11}<<",
        ">>{:11}<<",
        "DEBUG {:>3} - {:<8} Entries: {:>2}",
        "DEBUG {:<2} {} P({:>3}) N({:>3}) Title({:<15}) Param({:<15})",
        "DEBUG: Node:{}, LoopIndex:{}, W-Start:{} W-End:{} SelectedIndex:{} ",
        "DEBUG: {:<2} {} P({:>3}) N({:>3}) Title({:<15})"
    ]
    
    
    # start = first menu entry to display
    # end = start + numberOfRows
    # selid = selected id of menue entry
    # index = current menue entry id
    
    _window = {"start":0,"end":0,"selected":0,"index":0, "last_entry":False}
    def __init__(self, numberOfRows, structure, draw_callback, ShowMenuLevel=True, ExitLabel = "EXIT"):
        # Initialize internal variables
        self._mnodes = MenuNode()
        self._call_draw = draw_callback
        self._numberOfRows = numberOfRows
        self._showMenuLevel = ShowMenuLevel
        self._exitLabel = ExitLabel

        for entry in structure:
            self._mnodes[entry[1]] = [entry[0], entry[2], entry[3], entry[4]]  
            #print ">>", entry[0]
            if isinstance(entry[3],int):
                if self._parentIDs.has_key(entry[3]) == False:
                    #                          parentId, currentLevelId
                    self._parentIDs[entry[3]]=[entry[1],entry[2]]
       
       
       
        #print self._mnodes
        #print self._parentIDs
        self._window["start"] = 0
        self._window["selected"] = 0
        self._window["last_entry"] = False
        
    def draw(self, levelId):
        '''
            calculate next menu entries which should be displayed.
            levelId is the current level which is displayed
        '''
        disp = []
        self._currentLevelId = levelId
        selChar = self._selectChar
        
        #
        # check if level is available inside menu structure
        # if not print a error message on display
        #
        if self._mnodes.has_key(levelId) == False:
            disp.append(self._displayString[self._errorDisp1].format(levelId))
            disp.append(self._displayString[self._errorDisp2].format("missing menu"))
            disp.append(self._displayString[self._errorDisp3].format("entry"))
            self._call_draw(disp)
            return
            
        count = len(self._mnodes[levelId])
        #
        # calculate how many entries should be displays
        # start/end are the display "window"
        self._window["end"] = self._window["start"] + self._numberOfRows - 1

        #
        # do we need a "header row"?
        #print self._mnodes[self._currentLevelId], " LEN:", count
        if (self._showMenuLevel):
            disp.append(self._displayString[self._headerDisp].format(
                        self._parentIDs[self._currentLevelId][0], 
                        self._parentIDs[self._currentLevelId][1],
                        count
                        ))
            self._window["end"] -= 1
        
        # if window end larger then last entry, set window end to last entry
        if (self._window["end"] > count-1):
            self._window["end"] = count-1
            
        nodeId = 0
        
        first = self._window["start"]
        last  = self._window["end"]
        #print first,"--",last
        subChar = ""
        # iterate through all menu entries in this level
        for entry in self._mnodes[self._currentLevelId]:           
            # use only these entries, which are "inside the window"
            if (nodeId >= first and nodeId <= last):
                if nodeId > first:
                    selChar=" "
                if isinstance(entry[2],int):
                    subChar=">"
                else:
                    subChar=""
                disp.append(self._displayString[self._firstDispEntry].format(
                    selChar, 
                    nodeId,
                    #self._currentLevelId, 
                    #entry[0], 
                    entry[1], 
                    subChar))
                    
            if (nodeId == (last+1) and nodeId >= count):
                disp.append(self._displayString[1].format(
                    selChar,
                    nodeId, 
                    #self._currentLevelId,
                    #entry[0],                    
                    self._exitLabel,
                    "")
                    )
            
            nodeId += 1
            pass
        
        nodeId += 1
        
#        disp.append(self._displayString[self._debugDisp3].format(nodeId, 
#                             0, 
#                             self._window["start"], 
#                             self._window["end"], 
#                             self._window["selected"])
#                    )  
#                    
#        print self._parentIDs
        self._call_draw(disp)
        pass

    

    def select(self):
        '''
            called if current entry should be selected and system jump
            into submenu or exec a callback function. This behaviour
            depends on configuration inside structure
            
        '''
        index = self._window["selected"]
        entry = self._mnodes[self.getCurrentLevelId()][index]
        #print "SELECT:", entry
        if isinstance(entry[2], int):
            # submenu available
            self.setCurrentLevelId(entry[2])
            x=0
            # - if EXIT is available delete it
            for e in self._mnodes[self.getCurrentLevelId()]:
                if e[3] == -999:
                    del self._mnodes[self.getCurrentLevelId()][x]
                x +=1
                
            self._window["last_entry"] = False
            self._window["start"] = 0
            self._window["end"] = 0
            self._window["selected"] = 0
            self.draw(entry[2])
        else:
            return ["{}_{}".format(self.getCurrentLevelId(), index ),entry[2],entry[1],entry[3]]
        pass       
    
    def getCurrentLevelId(self):
        return self._currentLevelId
        
    def setCurrentLevelId(self, id):
        '''
            set current level id 
        '''
        if id < 0: 
            id = 0
        self._currentLevelId = id
        
    def down(self, levelId = None):
        '''
            set next level menu entry to the selected entry.
            If last entry displayed, system concatinate a "exitLabel"
            to current submenu. This exitLabel can be used to go back to
            parent menu
        '''
        w_end = len(self._mnodes[self.getCurrentLevelId()]) - 1# - self._numberOfRows
        if (len(self._mnodes[self.getCurrentLevelId()]) < self._numberOfRows):
            lessEntries = True
        else:
            lessEntries = False
        if (self._showMenuLevel):
            w_end -= 1
            
        if (self._window["start"] <= w_end):
            self._window["start"] += 1
         
        if ((w_end == self._window["end"]) or ((w_end <= self._window["end"]) and lessEntries)):
            if self._window["last_entry"] == False:
                self._mnodes[self.getCurrentLevelId()].append(
                    [self._parentIDs[self.getCurrentLevelId()][0],
                    self._exitLabel, self._parentIDs[self.getCurrentLevelId()][0],-999])
            self._window["last_entry"] = True
        #print "Down Start: ", self._window["start"], "-- W-End: ", w_end
        self._window["selected"] = self._window["start"]
        if levelId == None:
            self.draw(self.getCurrentLevelId())
        else:
            self.draw(levelId)
            
        pass
    
    def up(self, levelId = None):
        self._window["start"] -= 1
        if self._window["start"]  < 0:
            self._window["start"]  = 0
            
        self._window["selected"] = self._window["start"]
        if levelId == None:
            self.draw(self.getCurrentLevelId())
        else:
            self.draw(levelId)
        pass
              