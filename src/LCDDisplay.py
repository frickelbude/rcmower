# -*- coding: utf-8 -*-
"""
Created on Sun Aug 16 17:35:11 2015

@author: bernhardklein
"""
import math
import time
from time import sleep

import Adafruit_CharLCD as LCD


class LCDDisplay(object):

    # Raspberry Pi pin configuration:
    lcd_rs        = 22  # Note this might need to be changed to 21 for older revision Pi's.
    lcd_en        = 27
    lcd_d4        = 25
    lcd_d5        = 24
    lcd_d6        = 23
    lcd_d7        = 18
    lcd_backlight = 4 

    MAX_COLS    = 16
    MAX_ROWS    = 4

    USER_CHAR = {
        "1_ARROW_RIGHT":[0x0,0x8,0xc,0xe,0xc,0x8,0x0,0x0],    
        "2_SUBMENU":[0x8,0xc,0xa,0x9,0xa,0xc,0x8,0x0],
        "1_HEART":[0x0,0x0,0xa,0x1f,0x1f,0xe,0x4,0x0],
        "2_HEART":[0x0,0x0,0x0,0xa,0xe,0x4,0x0,0x0],
        "5_NULL":[0,0x01,0x02,0x03,0x03,0x4,0,0]
    }
    
    def __init__(self):
        self.lcd = LCD.Adafruit_CharLCD(self.lcd_rs, self.lcd_en, \
                                        self.lcd_d4, self.lcd_d5, \
                                        self.lcd_d6, self.lcd_d7, \
                                        self.MAX_COLS, self.MAX_ROWS
                                        , \
                                        self.lcd_backlight)
        #self.lcd.autoscroll(True); 
        self.lcd.clear()                              
        self._setUserChars()
        self.lcd.message("LCD initilizing ")
        pass
    
    def _setUserChars(self):
        loc=0
        #self.lcd.create_char(0,self.USER_CHAR["ARROW_RIGHT"])
        for x in range(0,7):
            self.lcd.create_char(x,[0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00])
            
        for key in self.USER_CHAR:
            #print key, self.USER_CHAR[key]
            self.lcd.create_char(loc, self.USER_CHAR[key])
            loc=loc+1
        pass
      
    def displayUserChars(self):
        uc = ['\x00','\x01','\x02','\x03','\x04','\x05','\x06','\x07']
        for x in range (0,5):            
            self.lcd.message(uc[x])
        #self.lcd.message('\x00')
        pass
    
    def setCursor(self, col, row):
        self.lcd.set_cursor(col,row)
        pass

    def writeChar(self, ch):
        self.lcd.write8(ord(ch), True)
        time.sleep(0.01)
        pass
     
    def clear(self):
        self.lcd.clear()
        pass
    
    def text(self, text):
        self.lcd.message(text)
        pass
    
    def message(self, text):
        self.lcd.message(text)
        pass
        
    def message(self, msg, row, clear=True):
        ''' Write text on row. Start at column 0. Ignore \n 
            col = 0 - MAX_COLS-1
            row = 0 - MAX_ROWS-1
        '''

        if clear:
            self.lcd.clear()        
        if row < 0:
            row = 0
        if row > self.MAX_ROWS:
            row = self.MAX_ROWS-1
        self.lcd.set_cursor(0,row)
        for c in msg:  
            self.lcd.write8(ord(c),True)

        pass
    

    
    
       