# -*- coding: utf-8 -*-
"""
Created on Wed Sep 16 13:48:08 2015

@author: bernhardklein
"""

import copy

class LCDSite(object):
    '''
    an LCDSite represent a number of rows which are used to display
    text and values
    
    structure is a array. Arraypostion = row number.
    Every entry can contain field entries (like {}). With update() it is 
    possible to fill data into this fields.
    
    update:
        do an update of this site - set new values
        
    draw:
        call function for displaying this site
        
    
    '''
    def __init__(self, siteId, structure, draw_callback):
        self._id = siteId
        self._side_struct = structure
        self._side_data = []
        self._draw = draw_callback
        self._site_data = copy.deepcopy(self._side_struct)
        pass
    
    
    def update(self, data_dict):
        '''
        data must be a dictionary with format:
            {row:[value1, value2,...], ...}

        
        '''
        self._site_data = copy.deepcopy(self._side_struct)
        if data_dict == None or len(data_dict) == 0:
            self._draw(["empty data_dict"])
            return
            
        #print "DataDict:", data_dict
        for key, value in data_dict.iteritems():
            #print "{} *** {}".format(key,value)
            row = self._site_data[key].format(d=value)
            self._site_data[key] = row
            #print ">> {} --- {}".format(key,row)
        pass
    
    def getData(self):
        return self._side_data
        
    def draw(self):
        '''
            callback function and use data list as parameter
        '''
        #print ">>>", self._site_data
        self._draw(self._site_data)
        pass
    
class LCDSites(object):
    '''
        Main class
        LCDSites handle different sites which could be displayed on an LCD
        device.
        
        A LCDsite represent a number of rows which are displayed at the same
        time
        
        The real painting on LCD is done outside this class via callback function
        to avoid problems with different devices.
        Like MVC pattern (the view(ing) is outside)
            MODELL      : class LCDSite
            VIEW        : outsite (should be done from YOU)
            CONTROLLER  : class LCDSites (this class)
        
    '''
    _lcdsites = {}
    _site=0
    def __init__(self, draw_callback):
        '''
        constructor
        '''
        self._draw = draw_callback
        
        pass
    
    def addNewSite(self, siteId, structure):
        '''
        add a new LCDsite, which can be displayed via draw()
        
        put this structure into internal list
        
        '''
        s = LCDSite(siteId, structure, self._draw)
        self._lcdsites.update({siteId:s})
        pass
    
    def removeSite(self, siteId):
        '''
        '''
        if self._lcdsites.has_key(siteId):
            del self._lcdsites[siteId]
        else:
            self._draw(["unknown siteId"])
            return -1
                
    def nextSite(self):
        if self._site < len(self._lcdsites):
            self._lcdsites[self._site].draw()
            self._site += 1
        else:
            self._site = 0

        pass
    
    def draw(self, siteId=0):
        '''
            call function and use give LCDsite with siteID
            
        '''
        if self._lcdsites.has_key(siteId):
            self._lcdsites[siteId].draw()
        else:
            self._draw(["unknown siteId"])
            return -1
            
    def updateSite(self, siteId, siteData, autoDraw=True ):
        '''
        update site with siteData.
        siteData must correspondent with site format. After successfuly
        update, the internal draw method is called
        '''
        if self._lcdsites.has_key(siteId):
            self._lcdsites[siteId].update(siteData)
            if autoDraw:
                self._lcdsites[siteId].draw()
            pass
        else:
            self._draw(["unknown siteId"])
            return -1
            