# README #

radio controlled mower (big mower). Mower based on a 12.5hp Briggs engine to drive double cutter bar and two 450W electro motors

Electronic:
Raspberry Pi as "brain"
Sony F710 controller for controlling the mower
2x 24v23 Pololu motor drivers
1x 100A shunt resitor for checking current
2x 12V 65/95A car batteries

Softare:
written in Python


Author:
Bernd Klein, 2014/2015